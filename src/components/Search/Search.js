import styles from './Search.module.css';
import Title from '../Title';
import Buttons from '../Buttons/Buttons';

function Search() {
  return (
    <div>
     <Title text="Search" type="h2" />
     
      <div className={styles.searchBox}>
      <input className={styles.forSearchInput} type="text" placeholder="Type to search" />
      <Buttons text="All" type="button"/>
      <Buttons text="Important" type="button"/>
      <Buttons text="Done" type="button"/>
      </div>
    </div>
  );
};
export default Search;
