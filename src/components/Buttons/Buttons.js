import styles from "./Buttons.module.css";

function getBut(type, text, onBtnClick) {
  switch (type) {
    case "button":
      return <button className={styles.forSearchButtons} onClick={onBtnClick}>{text}</button>;
    case "edit":
      return <button onClick={onBtnClick} className={styles.Edit}>{text}</button>;
    case "important":
      return <button  onClick={onBtnClick} className={styles.Important}>{text}</button>;
    case "done":
      return <button  onClick={onBtnClick} className={styles.Done}>{text}</button>;
    case "delete":
      return <button  onClick={onBtnClick} className={styles.Delete}>{text}</button>;
      default: <button  onClick={onBtnClick}>{text}</button>;
  }
}

function Buttons({ text, type = "button", onBtnClick}) {
  return <>{getBut(type, text, onBtnClick)}</>;
}
export default Buttons;

