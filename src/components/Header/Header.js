import Title from "../Title";
import styles from "./Header.module.css"

function Header() {
  return (
    <div>
      <Title text="Header" type="h1"/>
      <div className={styles.headerInput}
      >
        <input
          className={styles.forInput}
          type="text"
          placeholder="What do you have planned?"
        />
        <button className={styles.forAddButton}>ADD TASK</button>
      </div>
    </div>
  );
};

export default Header;
