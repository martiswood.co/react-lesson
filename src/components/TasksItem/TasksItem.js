import styles from "./TasksItem.module.css";
import Buttons from "../Buttons/Buttons";
import React from "react";

export class TasksItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      title: props.task.title,
      isImportant: props.task.important,
      isDone: props.task.done,
    };
  }

  onEditClick = () => {
    this.setState({
      edit: !this.state.edit,
    });
  };

  onImportantClick = () => {
    this.setState({
      isImportant: !this.state.isImportant,
    });
  };

  onDoneClick = () => {
    this.setState({
      isDone: !this.state.isDone,
    });
  };

  handleChange = (event) => {
    this.setState({ title: event.target.value });
  };

  render() {
    const { id } = this.props.task;
    const editButtonText = this.state.edit ? "Save" : "Edit";

    return (
      <li className={styles.listItem} key={id}>
        {this.state.edit ? (
          <input
            onChange={this.handleChange}
            value={this.state.title}
            className={styles.listItemName}
          />
        ) : (
          <p
            className={`${styles.listItemName} ${styles.title} 
             ${this.state.isImportant ? styles.purple : ""}
             ${this.state.isDone ? styles.green : ""}
             `}
          >
            {this.state.title}
          </p>
        )}
        <Buttons
          onBtnClick={this.onEditClick}
          text={editButtonText}
          type="edit"
        />
        <Buttons
          onBtnClick={this.onImportantClick}
          text="Important"
          type="important"
        />
        <Buttons 
        onBtnClick={this.onDoneClick} 
        text="Done" 
        type="done" 
        />
        <Buttons text="Delete" type="delete" />
      </li>
    );
  }
}
