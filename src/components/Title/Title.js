import styles from "./Title.module.css";

function getHead(CustomTag, text) {

  return <CustomTag className={styles.custom}>{text}</CustomTag>
  }

function Title({ text, type = "h1" }) {
  return (
    <>
      {getHead(type,text)}
      <hr className={styles.searchHr} />
    </>
  );
}
export default Title;

