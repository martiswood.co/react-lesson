import styles from "./Tasks.module.css";
import TasksItem from "../TasksItem";
import Title from "../Title";
import { useSelector } from 'react-redux';
import {selectList} from "../../store/listSlice"

function Tasks() {
  const toDoList = useSelector(selectList);
  return (
    <div>
      <Title text="Tasks" type="h3"/>
      <ul className={styles.list}>
        {toDoList.map((task) => (
          <TasksItem task={task} key={task.id}/>
        ))}
      </ul>
    </div>
  );
}

export default Tasks;

