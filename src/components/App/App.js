import Tasks from "../Tasks/Tasks";
import Search from "../Search/Search";
import Header from "../Header/Header";

import styles from "./App.module.css";


function App() {
    return(
      <div className={styles.container}>
        <Header />
        <Search />
        <Tasks />
      </div>
    )
  }

  export default App

