import { createSlice } from '@reduxjs/toolkit'

export const listSlice = createSlice({
  name: 'list',
  initialState: [
    { id: 1, title: "learn html and css", important: false, done: false },
    { id: 2, title: "learn javascript", important: true, done: false },
    { id: 3, title: "learne html and css", important: false, done: true },
    { id: 4, title: "learne html and css", important: true, done: false },
  ],
  reducers: {
  },
})

export default listSlice.reducer
export const selectList = (state) => state.list